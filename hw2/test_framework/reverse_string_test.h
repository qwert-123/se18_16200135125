#pragma once
#include <string>
using std::string;
class ReverseStringTest
{
public:
	static void testReverseNormal();
	static void testReverseBlank();
private:
	static void stringToCharArray(char *str, string& testWord);
};
