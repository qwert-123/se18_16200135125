#include <iostream>
#include "reverse_string.h"
using std::cout;
using std::endl;

void reverse(char *str)
{
     //index first and last, exchange
     if(str == NULL)
          return;
          
     //find end
     char* end = str;
     while(*end)
     {
          ++end;
     }
     --end;
     char* start = str;
     char tmp;

     //loop to exchange
     while(start < end)
     {
          tmp = *start;
          *start++ = *end;
          *end-- = tmp;
     }
}

