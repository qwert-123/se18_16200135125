#include <iostream>
#include <string>
#include "assert.h"
#include "reverse_string.h"
#include "reverse_string_test.h"
using std::string;
using std::cout;
using std::endl;

void ReverseStringTest::testReverseNormal()
{
	string testWord = "abc";
	string oriResWord = "cba";
	
	char str[testWord.size() + 1] ;
	stringToCharArray(str, testWord);
	
	//调用函数，调用函数之后，str里存着反转之后的结果 
	reverse(str);
	
	//将char[]类型构造回string类型 
	string testResWord(str);
	
	//检测测试结果和预期结果是否一致 ，若不一致则会退出 
	assertEqual(testResWord, oriResWord);
	
	//若通过检测，则输出测试通过信息 
	cout << "testReverseNormal() OK!" << endl;
}

void ReverseStringTest::testReverseBlank()
{
	string testWord = "";
	string oriResWord = "";
	
	char str[testWord.size() + 1] ;
	stringToCharArray(str, testWord);
	
	reverse(str);
	
	string testResWord(str);
	
	assertEqual(testResWord, oriResWord);
	
	cout << "testReverseBlank() OK!" << endl;
}

/*private method below*/

void ReverseStringTest::stringToCharArray(char *str, string& testWord)
{
	if(str == NULL)
		return;
		
	//将string类型的"abc"构造成char[]字符数组类型 	
	for (int i = 0; i < testWord.size(); i++)
	{
		str[i] = testWord[i];
	}
	str[testWord.size()] = '\0'; //最后插入字符串结束符'\0'
}
